# json-emulator

***Emulator for sending JSONs***

JSON consists of iformation about temperature, humidity and light intensity of terrarium

Version 1.0: Script sends a JSON to http server 
(to change the server url edit the code using Python IDLE)

Version 2.0: This program sends JSONs to http server with time interval 
(url and time between sending can be set up in options) 

To run the script you should only download [Python IDLE](https://www.python.org/downloads/)