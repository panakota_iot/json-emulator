# Created by Panakota
import time
import sys
import urllib.request
import json
import random

time_between_sending_json = 1
server_url = "http://192.168.88.51:8080/"

def json_generating():
    try:
        while (True):
            temperature = round(random.triangular(22, 25), 1)
            humidity = round(random.triangular(60, 65), 1)
            light = random.randint(100, 115)
            mess = {
                "Temperature": temperature,
                "Humidity": humidity,
                "Light": light
            }
            req = urllib.request.Request(server_url)
            req.add_header('Content-Type', 'application/json; charset=utf-8')
            json_data = json.dumps(mess)
            json_data_in_bytes = json_data.encode('utf-8')  # needs to be bytes
            req.add_header('Content-Length', len(json_data_in_bytes))
            print(json_data_in_bytes)
            response = urllib.request.urlopen(req, json_data_in_bytes)
            time.sleep(time_between_sending_json)
    except urllib.error.URLError:
        print("Connection Error")

def main():
    print("\t\tEmulator")

    is_chosen = False
    while (not is_chosen):
        print("""\n0 - exit\n1 - start generating JSON\n2 - Options""")
        print("Enter your mode: ", end="")
        choice = int(input())

        if choice == 0:
            print("Bye")
            is_chosen = True
            sys.exit()
        elif choice == 1:
            is_chosen = True
            json_generating()
        elif choice == 2:
            print("Enter new server url: ", end="")
            server_url = input()
            print("Enter new time between sending JSONs: ", end="")
            time_between_sending_json = int(input())

main()
input("\n\nPress the Enter key to exit...")
