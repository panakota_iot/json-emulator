import urllib.request
import json

mess = {
    "Temperature": 22.5,
    "Humidity": 60.3,
    "Light": 115
}

server_url = "http://192.168.88.51:8080/"
req = urllib.request.Request(server_url)
req.add_header('Content-Type', 'application/json; charset=utf-8')
json_data = json.dumps(mess)
json_data_in_bytes = json_data.encode('utf-8')   # needs to be bytes
req.add_header('Content-Length', len(json_data_in_bytes))
print(json_data_in_bytes)
response = urllib.request.urlopen(req, json_data_in_bytes)